



function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

function setAuth(callback) {
	var xhr = new XMLHttpRequest();
	xhr.open("post", "http://localhost:9000/authtoken",  true);
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4) {
			// json.parse does not evaluate the attacker's scripts.
			var resp = JSON.parse(xhr.responseText);
			createCookie("token", resp.user._id, 300);
			callback(resp.user._id)
		}
	}
	xhr.send();
}

function makecalltoapi() {
  auth = readCookie("token");
  if (auth == null) {
    setAuth(getBar);
  } else {
    getBar(auth);
  }
}

function init() {
  var location = readCookie("location");
  if (location == null) {
    navigator.geolocation.getCurrentPosition(function(position) {
        getLocationName(position.coords.latitude, position.coords.longitude);
    });
  } else {
    makecalltoapi();
  }
}

function include(obj) {
    var arr = ["Berlin", "Amsterdam", "London", "Sao Paulo", "Paris", "Toronto", "Sydney", "Melbourne", "San Francisco", "New York"];
   return (arr.indexOf(obj) != -1);
} 

 function getLocationName(lat, lon) {
    var geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(lat, lon);
    geocoder.geocode( { 'location': latlng}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          for (var i = 0; i < results.length; i++) {
            if (results[i].types[0] === "locality") {
              var city = results[i].address_components[0].short_name;
              if (include(city) == false){
                $("#popUpBox").removeClass("hide");
              } 
              createCookie("location", city, 300);
              makecalltoapi();
              }
            }
          }
        });
    }

function getBar(auth) {
	var xhr = new XMLHttpRequest();
	var url;
	if (include(readCookie("location")) == true) {
	   url = "http://localhost:9000/"+auth+"/bars?location="+readCookie("location")
	} else {
	   url = "http://localhost:9000/"+auth+"/bars"
	}
	xhr.open("get", url,true);
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4) {
			// json.parse does not evaluate the attacker's scripts.
			var resp = JSON.parse(xhr.responseText);

			$("#title").html(resp.title);
			$("#site").attr("href", resp.url);

			$("body").css("background", "url("+resp.photourl+") no-repeat center center fixed")
			$("body").css("background-size", "cover")

			var _t = "Check out this '"+ resp.title + "' - "+ resp.url + " - Discover Great Bars with every new tab";
      			$("#directions").attr('href', "http://maps.google.com?q="+resp.address);
      			$("#events").attr('href', resp.eventsurl);
			$("#twitter").attr("href", "https://twitter.com/intent/tweet?url=http://getbartab.com/&text="+_t+"&via=getbartab");
			$("#facebook").attr("href", "http://www.facebook.com/sharer.php?summary="+_t+"&u="+resp.url)

			$('#twitter,#facebook').click(function(event) {
					var width  = 575,
					height = 400,
					left   = ($(window).width()  - width)  / 2,
					top    = ($(window).height() - height) / 2,
					url    = this.href,
					opts   = 'status=1' +
					',width='  + width  +
					',height=' + height +
					',top='    + top    +
					',left='   + left;
					window.open(url, 'twitter', opts);
					return false;
			});

			$(".switch-button").unbind().bind("click", function(){
				getBar(auth);
			});

			function error(err) {
				console.warn('ERROR(' + err.code + '): ' + err.message);
			};

			$('li', 'ul.dropdown-menu').unbind().bind("click", function(){
				var value = $(this).attr("ref");
                $("#city-select").empty().append("<img src='images/icon/berlin.svg'>");
                $("#city-select").append("<p>"+value+"</p>");
				createCookie("location", value, 300);
				getBar(auth);
			});
		}
	}
	xhr.send();
}

init();






